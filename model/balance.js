
const mongoose = require('mongoose');

/**
 * Balance
 *
 * Wallet balances.
 */
const Balance = mongoose.model('Balance', new mongoose.Schema({
  __v: { select: false, type: Number },
  _id: { select: true, required: true, type: String },
  balance: { default: 0.0, index: true, required: true, type: Number },
}, { versionKey: false }), 'balances');

module.exports =  Balance;
