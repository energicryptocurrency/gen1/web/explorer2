
const mongoose = require('mongoose');
const UTXO = require('./utxo');

const unindexed_utxo_schema = UTXO.schema.clone();

// A dirty hack
unindexed_utxo_schema.paths.address._index = false;
unindexed_utxo_schema.paths.blockHeight._index = false;
unindexed_utxo_schema._indexes.length = 0;
unindexed_utxo_schema._indexedpaths = undefined;

if (unindexed_utxo_schema.indexes().length > 0) {
  throw new Error("Not indexes must be present!");
}

/**
 * STXO
 *
 * Spent transactions in the blockchain.
 */
const STXO = mongoose.model('STXO', new mongoose.Schema({
  __v: { select: false, type: Number },
  _id: { required: true, select: false, type: String },
  address: { index: true, required: true, type: String },
  blockHeight: { index: true, required: true, type: Number },
  n: { required: true, type: Number },
  txId: { required: true, type: String },
  value: { required: true, type: Number },
  utxo: { required: true, type: unindexed_utxo_schema, select: false },
}, { versionKey: false }), 'stxo');

module.exports =  STXO;
