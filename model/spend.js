
const mongoose = require('mongoose');

/**
 * Spend
 *
 * Wallet spends.
 */
const Spend = mongoose.model('Spend', new mongoose.Schema({
  __v: { select: false, type: Number },
  _id: { select: true, required: true, type: String },
  spent: { default: 0.0, required: true, type: Number },
}, { versionKey: false }), 'spends');

module.exports =  Spend;
