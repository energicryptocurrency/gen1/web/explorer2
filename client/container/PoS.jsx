
import Actions from '../core/Actions';
import moment from 'moment';
import blockchain from '../../lib/blockchain';
import Component from '../core/Component';
import { connect } from 'react-redux';
import numeral from 'numeral';
import PropTypes from 'prop-types';
import React from 'react';

import HorizontalRule from '../component/HorizontalRule';
import Select from '../component/Select';

class PoS extends Component {
  static defaultProps = {
    avgBlockTime: 60,
    hashps: 0
  };

  static propTypes = {
    // Dispatch
    getCoins: PropTypes.func.isRequired,

    avgBlockTime: PropTypes.number.isRequired,
    hashps: PropTypes.number.isRequired,
    coin: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      coins: [],
      amount: 0.0,
      mn: 0.0,
      mns: 'None',
      pos: 0.0
    };
  };

  componentDidMount() {
    this.getAmount();
    Promise.all([
        this.props.getCoins(),
      ])
      .then((res) => {
        this.setState({
          coins: res[0], // 7 days at 5 min = 2016 coins
          loading: false,
        });
      });
  };

  componentDidUpdate(prevProps) {
    if (this.props.match.params.amount !== prevProps.match.params.amount) {
      this.setState({
        mn: 0.0,
        mns: 'None',
        pos: 0.0
      }, this.getAmount);
    }
  };

  getAmount() {
    const { params: { amount } } = this.props.match;
    if (!!amount && !isNaN(amount) && amount > 0) {
      const { mn, pos } = this.getRewardSplit(amount);
      this.setState({ amount, mn, pos });
    } else {
      this.setState({ error: 'Please provide an amount for staking calculations.' });
    }
  };

  getRewardSplit = (amount) => {
    let mn = 0;
    let pos = amount;

    if (this.state.mns !== 'None') {
      mn = this.state.mns * blockchain.mncoins;
      pos = amount - mn;
    }

    return { mn, pos };
  };

  getRewardHours = (pos) => {
    if (!pos || pos < 0) {
      return 0.0;
    }
    var difficulty = this.props.coin.diff 
    var hash = this.props.coin.netHash;


    // calculatest highestHashPerDay
    const hashes = new Map();
    this.state.coins.forEach((c, idx) => {
      const k = moment(c.createdAt).format('MMM DD');

      if (hashes.has(k)) {
        hashes.set(k, hashes.get(k) + c.netHash);
      } else {
        hashes.set(k, c.netHash);
      }
    });
    // Generate averages for each key in each map.
    const l = (24 * 60) / 5; // How many 5 min intervals in a day.
    let highestHashPerDay = 0.0;
    hashes.forEach((v, k) => {
      const hash = (v / l);
      if (hash > highestHashPerDay) {
        highestHashPerDay = hash;
      }
    });
    // console.log('highestHashPerDay', highestHashPerDay)


    // set to 1.8M total staking if under 1.8M NRG total staking as per Tommy
    if (highestHashPerDay < (1000000*1000000*1.8)) {
      hash = 1000000*1000000*1.8
    }
    // console.log('highest hash', hash)
    var avgBlockTime = this.props.coin.avgBlockTime
    var totalNrgStaking = hash / 1000000;
    var rewardPerHour = (3600/avgBlockTime) * (pos/totalNrgStaking);
    // add const 1 hour as per Andrey
    var hours = 1/rewardPerHour + 1
    return hours;
  };

  renderMasternodeCount = () => {
    const mns = Math.floor(this.state.amount / blockchain.mncoins);
    const options = [];
    for (let i = 0; i <= mns; i++) {
      options.push({ label: !i ? 'None' : i, value: !i ? 'None' : i });
    }

    return (
      <div
        style={ this.state.mns < 1
          ? { marginBottom: 5, marginTop: -7 }
          : { marginBottom: 5, marginTop: -9 }
        }>
        <Select
          onChange={ v => this.setState({ mns: parseInt(v, 10) || 'None' }, this.getAmount) }
          selectedValue={ this.state.mns }
          options={ options } />
      </div>
    );
  };

  getX = () => {
    const subsidy = blockchain.getSubsidy(this.props.coin.blocks + 1) - 9.14 - 2.28; // 9.14 is for treasury, 2.28 is for backbone
    const mnSubsidy = blockchain.getMNSubsidy(this.props.coin.blocks + 1);
    const posSubsidy = subsidy - mnSubsidy;

    let pos = this.state.amount;
    let mn = 0.0;
    if (this.state.mns !== 'None') {
      mn = this.state.mns * blockchain.mncoins;
      pos -= mn;
    }

    return {
      mn,
      mnHours: this.props.coin.avgMNTime,
      mnSubsidy,
      pos,
      posHours: this.getRewardHours(pos),
      posSubsidy,
      subsidy
    }
  };

  getDay = () => {
    const x = this.getX();

    if (x.mnHours !== 24.0 && x.mnHours > 0) {
      x.mnSubsidy = (24.0 / x.mnHours) * x.mnSubsidy;
    } else if (x.mnHours <= 0) {
      x.mnSubsidy = 0.0;
    }
    x.mnHours = 24.0;

    if (x.posHours !== 24.0 && x.posHours > 0) {
      x.posSubsidy = (24.0 / x.posHours) * x.posSubsidy;
    } else if (x.posHours <= 0) {
      x.posSubsidy = 0.0;
    }
    x.posHours = 24.0;

    return x;
  };

  getWeek = () => {
    const x = this.getDay();

    x.mnHours *= 7;
    x.mnSubsidy *= 7;
    x.posHours *= 7;
    x.posSubsidy *= 7;
    x.subsidy *= 7;

    return x;
  };

  getMonth = () => {
    const x = this.getDay();

    x.mnHours *= 30;
    x.mnSubsidy *= 30;
    x.posHours *= 30;
    x.posSubsidy *= 30;
    x.subsidy *= 30;

    return x;
  };

  render() {
    if (!!this.state.error) {
      return this.renderError(this.state.error);
    }

    const vX = this.getX();
    const vDay = this.getDay();
    const vWeek = this.getWeek();
    const vMonth = this.getMonth();

    let mns = 0.0;
    if (this.state.mns !== 'None') {
      mns = this.state.mns;
    }

    return (
      <div>
        <HorizontalRule title="PoS Calculations" />
        <p>
          Please note the following estimations are based on current block height,
          average block time, average masternode reward time, and current block reward schedule.
        </p>
        <br />
        <div className="row">
          <div className="col-sm-4">
            <b>Block Subsidy:</b>
          </div>
          <div className="col-sm-8">
            { numeral(vX.subsidy).format('0,0.0000') } NRG
          </div>
          <div className="col-sm-4">
            <b>PoS:</b>
          </div>
          <div className="col-sm-8">
            { numeral(vX.posSubsidy).format('0,0.0000') } NRG
          </div>
          <div className="col-sm-4">
            <b>Masternode:</b>
          </div>
          <div className="col-sm-8">
            { numeral(vX.mnSubsidy).format('0,0.0000') } NRG
          </div>
          <div className="col-sm-4">
            <b>Calculation Amount:</b>
          </div>
          <div className="col-sm-8">
            { numeral(this.state.amount).format('0,0.0000') } NRG
          </div>
        </div>
        <hr />
        <br />
        <div className="row">
          <div className="col-sm-12 col-md-4">
            Masternode(s):
          </div>
          <div className="col-sm-12 col-md-2">
            { this.renderMasternodeCount() }
          </div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-4"></div>
          <div className="col-sm-12 col-md-2">
            <small className="text-gray">X</small>
          </div>
          <div className="col-sm-12 col-md-2">
            <small className="text-gray">Day</small>
          </div>
          <div className="col-sm-12 col-md-2">
            <small className="text-gray">Week</small>
          </div>
          <div className="col-sm-12 col-md-2">
            <small className="text-gray">Month</small>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-4">
            Masternode Amount (NRG):
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vX.mn).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-4">
            Masternode Reward Interval (Hours):
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vX.mnHours).format('0,0.00') }
          </div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-4">
            Masternode Reward (NRG):
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vX.mnSubsidy * mns).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vDay.mnSubsidy * mns).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vWeek.mnSubsidy * mns).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vMonth.mnSubsidy * mns).format('0,0.0000') }
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-sm-12 col-md-4">
            PoS Amount (NRG):
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vX.pos).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-4">
            PoS Reward Interval (Hours):
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vX.posHours).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
          <div className="col-sm-12 col-md-2"></div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-4">
            PoS Reward (NRG):
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vX.posSubsidy).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vDay.posSubsidy).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vWeek.posSubsidy).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vMonth.posSubsidy).format('0,0.0000') }
          </div>
        </div>
        <hr />
        <br />
        <div className="row">
          <div className="col-sm-12 col-md-4">
            Total Amount (NRG):
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vX.mnSubsidy * mns + vX.posSubsidy).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vDay.mnSubsidy * mns + vDay.posSubsidy).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vWeek.mnSubsidy * mns + vWeek.posSubsidy).format('0,0.0000') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral(vMonth.mnSubsidy * mns + vMonth.posSubsidy).format('0,0.0000') }
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-4">
            Total Amount (USD):
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral((vX.mnSubsidy * mns + vX.posSubsidy) * this.props.coin.usd).format('$0,0.00') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral((vDay.mnSubsidy * mns + vDay.posSubsidy) * this.props.coin.usd).format('$0,0.00') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral((vWeek.mnSubsidy * mns + vWeek.posSubsidy) * this.props.coin.usd).format('$0,0.00') }
          </div>
          <div className="col-sm-12 col-md-2">
            { numeral((vMonth.mnSubsidy * mns + vMonth.posSubsidy) * this.props.coin.usd).format('$0,0.00') }
          </div>
        </div>
      </div>
    );
  };
}

const mapDispatch = dispatch => ({
  getCoins: () => Actions.getCoinsWeek(dispatch),
});

const mapState = state => ({
  coin: state.coins && state.coins.length
    ? state.coins[0]
    : { avgMNTime: 0.0, usd: 0.0 }
});

export default connect(mapState, mapDispatch)(PoS);
