
import PropTypes from 'prop-types';
import React from 'react';

import Card from './Card';
import Icon from '../Icon';

export default class CardPoSCalc extends React.Component {
  constructor(props) {
    super(props);
    this.input = null;
    this.state = { amount: 0.0 };
  };

  handleClick = () => {
    const v = this.state.amount;

    if (!!v && !isNaN(v) && v > 0) {
      document.location.href = `/#/pos/${ v }`;
      document.getElementById("posCalc").scrollIntoView()
    } else {
      this.input.focus();
    }
  };

  handleKeyPress = (ev) => {
    if (ev.key === 'Enter') {
      ev.preventDefault();
      this.handleClick();
    } else {
      this.setState({ amount: ev.target.value });
    }
  };

  render() {
    return (
      <Card title="Staking Calculator">
      <div style={{fontWeight: 'bold', fontSize: 'medium'}}>The amount of Energi you will stake:</div>
        <div className="row" style={{display: 'flex', justifyContent: 'center'}}>
          <div className="col-sm-12 col-md-8">
            <input
              onClick={ this.handleClick }
              onKeyPress={ this.handleKeyPress }
              onChange={ ev => this.setState({ amount: ev.target.value })}
              ref={ i => this.input = i }
              style={{ width: '100%', borderRadius: '50px' }}
              type="text"
              value={ this.state.amount } 
              />
          </div>
          <div>
            <button className="btn btn-success" style={{borderRadius: '50px', color: '#ffffff', borderColor: '#0a9c7a', backgroundColor: '#0bb48d'}} onClick={ this.handleClick }>
              Calculate
            </button>
          </div>
        </div>
        <div className="row">
          <div style={{fontSize: 'small'}} className="col-sm-12 text-gray">
            * Calculates what returns you will get staking your Energi.
          </div>
        </div>
      </Card>
    );
  };
}
