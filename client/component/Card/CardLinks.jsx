
import React from 'react';

import Card from './Card';

const CardLinks = () => (
  <Card title="Links">
    <a href="https://www.energi.world/" target="_blank">Website</a><br />
    <a href="https://bitcointalk.org/index.php?topic=4912743" target="_blank">Bitcointalk</a><br />
    <a href="https://github.com/energicryptocurrency" target="_blank">Github</a><br />
    <a href="https://www.reddit.com/r/energicryptocurrency/" target="_blank">Reddit</a><br />
    <a href="https://discordapp.com/invite/sCtgNC3" target="_blank">Discord</a><br />
    <a href="https://t.me/energicrypto" target="_blank">Telegram</a><br />
    <a href="https://twitter.com/Energicrypto" target="_blank">Twitter</a><br />
    <a href="https://www.facebook.com/Energi-Cryptocurrency-195355164580336/" target="_blank">Facebook</a>
  </Card>
);

export default CardLinks;
