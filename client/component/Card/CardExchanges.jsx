
import React from 'react';

import Card from './Card';

const CardExchanges = () => (
  <Card title="Exchanges">
    <a href="https://www.kucoin.com/trade/NRG-BTC" target="_blank">Kucoin</a><br />
    <a href="https://wallet.crypto-bridge.org/market/BRIDGE.NRG_BRIDGE.BTC" target="_blank">CryptoBridge</a><br />
    <a href="https://www.coinexchange.io/market/NRG/BTC" target="_blank">Coinexchange</a><br />
    <a href="https://bitbns.com/trade/#/nrg" target="_blank">BitBns</a><br />
    <a href="https://www.digifinex.com/en-ww/trade/BTC/NRG" target="_blank">Digifinex</a><br />
    <a href="https://www.bitladon.com/energi" target="_blank">Bitladon</a><br />
  </Card>
);

export default CardExchanges;
