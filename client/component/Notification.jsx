import React from 'react';
import Component from '../core/Component';
import PropTypes from 'prop-types';

class Notification extends Component {
  static defaultProps = {
    message: ''
  };

  static propTypes = {
    message: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
  };

  render() {
    return (
      <div className="alert alert-primary pulse text-center" style={{ fontSize: '1em' }}>
		    <div style={{ fontSize: '1.25em', fontWeight: 'bold' }}>
		      {this.props.message}
		    </div>
		  </div>
    );
  }
}

export default Notification;
