const APIdata = [
  {
    heading: 'API Calls',
    subHeading: 'Return data from coind',
    calls: [
        {
          name: 'getAddress [hash]',
          info: 'Returns information for given address.',
          path: '/api/address/Ea5i3sG6SPHon5zD1vC3cSaKijLSoGh2ti'
        },
        {
          name: 'getBlock [height]',
          info: 'Returns block information for the given hash or height.',
          path: '/api/block/500'
        },
        {
          name: 'getBlockAverage',
          info: 'Returns the average block time over 24 hours.',
          path: '/api/block/average'
        },
        {
          name: 'getCoin',
          info: 'Returns coin information.',
          path: '/api/coin/'
        },
        {
          name: 'getCoinHistory',
          info: 'Returns the coin history.',
          path: '/api/coin/history'
        },
        {
          name: 'getMasternodes',
          info: 'Returns masternode information.',
          path: '/api/masternode'
        },
        {
          name: 'getMasternodeByAddress',
          info: 'Returns masternode information by Wallet Address.',
          path: '/api/masternode/EXvst5CZLo1UngMLu6gqi8ppzpp9P7BBZZ'
        },
        {
          name: 'getMasternodeCount',
          info: 'Returns masternodes enabled and total counts.',
          path: '/api/masternodecount'
        },
        {
          name: 'getMasternodeAverage',
          info: 'Returns the average payment for a masternode vs 24 hours.',
          path: '/api/masternode/average'
        },
        {
          name: 'getPeer',
          info: 'Returns peer information.',
          path: '/api/peer'
        },
        {
          name: 'getSupply',
          info: 'Returns circulating and total supply information.<br />https://github.com/coincheckup/crypto-supplies',
          path: '/api/supply'
        },
        {
          name: 'getTop100',
          info: 'Returns top 100',
          path: '/api/top100'
        },
        {
          name: 'getTXs',
          info: 'Returns transaction information.',
          path: '/api/tx'
        },
        {
          name: 'getTXsWeek',
          info: 'Request the transactions for a week.',
          path: '/api/tx/week'
        },
        {
          name: 'getTXLatest',
          info: 'Returns latest transaction information.',
          path: '/api/tx/latest'
        },
        {
          name: 'getTX [hash]',
          info: 'Returns information for the given transaction.',
          path: '/api/tx/562d8352a5de28a821ba01d3eee78aefda0786b6e44990c2cb91d24e658b7c58'
        },
        {
          name: 'getDifficulty',
          info: 'Returns the current difficulty.',
          path: '/api/getdifficulty'
        },
        {
          name: 'getConnectionCount',
          info: 'Returns the number of connections the block explorer has to other nodes.',
          path: '/api/getconnectioncount'
        },
        {
          name: 'getBlockCount',
          info: 'Returns the current block index.',
          path: '/api/getblockcount'
        },
        {
          name: 'getNetworkHashPS',
          info: 'Returns the current network hashrate. (hash/s)',
          path: '/api/getnetworkhashps'
        },
        {
          name: 'getMoneySupply',
          info: 'Returns the current money supply.',
          path: '/api/getmoneysupply'
        },
        // { name: 'getdistribution',
        //   info: 'Returns the number of connections the block explorer has to other nodes.',
        //   path: '/ext/getdistribution'
        // },
        {
          name: 'getAddress [hash]',
          info: 'Returns address information.',
          path: '/api/getaddress/Ea5i3sG6SPHon5zD1vC3cSaKijLSoGh2ti'
        },
        {
          name: 'getBalance [hash]',
          info: 'Returns the current balance.',
          path: '/api/getbalance/Ea5i3sG6SPHon5zD1vC3cSaKijLSoGh2ti'
        },
        {
          name: 'getLastTXs',
          info: 'Returns the last transactions.',
          path: '/api/getlasttxs'
        }
    ]
  },
  // {
  //   heading: 'Betting API',
  //   subHeading: 'Return data from local indexes',
  //   calls: [
  //     {
  //       name: 'getBetEvents',
  //       info: 'Returns all the bet events or bet event with specific id with query string eventId',
  //       path: '/api/bet/events?eventId=%23061'
  //     },
  //     {
  //       name: 'getBetActions',
  //       info: 'Returns all the bet actions or bet action with specific id with query string eventId',
  //       path: '/api/bet/actions?eventId=%23061'
  //     },
  //     {
  //       name: 'getBetResults',
  //       info: 'Returns all the bet results or bet result with specific id with query string eventId',
  //       path: '/api/bet/results?eventId=%23061'
  //     }
  //   ]
  // },
  // {
  //   heading: 'Extended API',
  //   subHeading: 'Return data from local indexes',
  //   calls: [
  //       {
  //         name: 'getMoneySupply',
  //         info: 'Returns the current money supply.',
  //         path: '/ext/getmoneysupply'
  //       },
  //       // { name: 'getdistribution',
  //       //   info: 'Returns the number of connections the block explorer has to other nodes.',
  //       //   path: '/ext/getdistribution'
  //       // },
  //       {
  //         name: 'getAddress',
  //         info: 'Returns address information.',
  //         path: '/ext/getaddress'
  //       },
  //       {
  //         name: 'getBalance',
  //         info: 'Returns the current balance.',
  //         path: '/ext/getbalance'
  //       },
  //       {
  //         name: 'getLastTXs',
  //         info: 'Returns the last transactions.',
  //         path: '/ext/getlasttxs'
  //       }
  //   ]
  // },
  {
    heading: 'Linking (GET)',
    subHeading: 'Linking to the block explorer',
    calls: [
        {
          name: 'Transaction (/#/tx/[hash])',
          info: 'Returns transaction information',
          path: '/#/tx/562d8352a5de28a821ba01d3eee78aefda0786b6e44990c2cb91d24e658b7c58'
        },
        {
          name: 'Block (/#/block/[height]',
          info: 'Returns block information.',
          path: '/#/block/500'
        },
        {
          name: 'Address (/#/address/[hash]',
          info: 'Returns address information.',
          path: '/#/address/Ea5i3sG6SPHon5zD1vC3cSaKijLSoGh2ti'
        },
        // { name: 'qr (qr/[hash]',
        //   info: 'Returns qr code information.',
        //   path: '/#/qr/000000000001eb792fe1ac3f901d2373509769f5179d9fe2fd3bf8cb3b6ebec9'
        // },
    ]
  }
]

export default APIdata;
