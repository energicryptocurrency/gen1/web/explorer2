
require('babel-polyfill');
const { exit, rpc } = require('../lib/cron');
const locker = require('../lib/locker');
// Models.
const Block = require('../model/block');
const Coin = require('../model/coin');
const Masternode = require('../model/masternode');
const Peer = require('../model/peer');
const Rich = require('../model/rich');
const TX = require('../model/tx');
const UTXO = require('../model/utxo');
const BetAction = require('../model/betaction');
const BetEvent = require('../model/betevent');
const BetPayout = require('../model/betpayout');
const BetResult = require('../model/betresult');
const ListEvent = require('../model/listevent');
const STXO = require('../model/stxo');
const Balance = require('../model/balance');
const Spend = require('../model/spend');

/**
 * Clear database.
 */
async function clearDatabase() {
  let models = [
    Block,
    Coin,
    Masternode,
    Peer,
    Rich,
    TX,
    UTXO,
    BetAction,
    BetEvent,
    BetPayout,
    BetResult,
    ListEvent,
    STXO,
    Balance,
    Spend,
  ];

  for (let m of models) {
    try {
      await m.collection.drop();
    } catch (e) {
      // pass
    }

    // Just in case
    await m.remove({});
  }
}

/**
 * Handle locking.
 */
async function update() {
  let code = 0;

  try {
    locker.lock('block');
    locker.lock('coin');
    locker.lock('masternode');
    locker.lock('peer');
    locker.lock('rich');
    locker.lock('tx');
    locker.lock('utxo');
    locker.lock('bet');
    locker.lock('listevent');
    await clearDatabase();
  } catch(err) {
    console.log(err);
    code = 1;
  } finally {
    try {
      locker.unlock('block');
      locker.unlock('coin');
      locker.unlock('masternode');
      locker.unlock('peer');
      locker.unlock('rich');
      locker.unlock('tx');
      locker.unlock('utxo');
      locker.unlock('bet');
      locker.unlock('listevent');
    } catch(err) {
      console.log(err);
      code = 1;
    }
    exit(code);
  }
}

update();
