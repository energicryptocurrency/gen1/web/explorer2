
require('babel-polyfill');
require('../lib/cron');
const config = require('../config');
const { exit, rpc } = require('../lib/cron');
const fetch = require('../lib/fetch');
const { forEach } = require('p-iteration');
const locker = require('../lib/locker');
const moment = require('moment');
// Models.
const Masternode = require('../model/masternode');

/**
 * Get a list of the mns and request IP information
 * from freegeopip.net.
 */
async function syncMasternode() {
  const date = moment().utc().startOf('minute').toDate();
  const d = new Date(0);

  await Masternode.remove({});

  // Increase the timeout for masternode.
  rpc.timeout(10000); // 10 secs

  const mns = await rpc.call('masternodelist', ['full']);
  var inserts = [];
  // console.log('mns',mns)
  // status protocol payee lastseen activeseconds lastpaidtime lastpaidblock IP
  // ENABLED 70208 Ee73NVsrNveHrKqLs3dM5hA8PXEoisPEdD 1541701934  4333963 1541698196 285593 45.77.228.153:9797'
  var masternodeArray = [];
  for (let key in mns) {
    var value = mns[key].trim();
    value = value.replace(/\s\s+/g, ' ');
    var splitString = value.split(" ")
    // excessive
    if (process.env.DEBUG_CRON) console.log('splitString', splitString);
    var json = {
      activetime: splitString[4],
      addr: splitString[2],
      lastseen: splitString[3],
      lastpaid: splitString[5],
      network: '1',
      rank: '1',
      status: splitString[0],
      txhash: '1',
      outidx: '1',
      version: splitString[1]
    }
    masternodeArray.push(json)
    // Excessive
    if (process.env.DEBUG_CRON) console.log('json', json);
  }
  await forEach(masternodeArray, async (mn) => {
  // await forEach(mns, async (mn) => {
    const masternode = new Masternode({
      active: mn.activetime,
      addr: mn.addr,
      createdAt: date,
      lastAt: d.setUTCSeconds(mn.lastseen),
      lastPaidAt: d.setUTCSeconds(mn.lastpaid),
      network: mn.network,
      rank: mn.rank,
      status: mn.status,
      txHash: mn.txhash,
      txOutIdx: mn.outidx,
      ver: mn.version
    });

    inserts.push(masternode);
  });

  if (inserts.length) {
    await Masternode.insertMany(inserts);
  }
  // Excessive
  if (process.env.DEBUG_CRON) console.log('inserts', inserts);
  console.log(`Inserted ${inserts.length}`);
}

/**
 * Handle locking.
 */
async function update() {
  const type = 'masternode';
  let code = 0;

  try {
    locker.lock(type);
    await syncMasternode();
  } catch(err) {
    console.log(err);
    code = 1;
  } finally {
    try {
      locker.unlock(type);
    } catch(err) {
      console.log(err);
      code = 1;
    }
    exit(code);
  }
}

update();
