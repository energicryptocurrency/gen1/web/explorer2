
require('babel-polyfill');
const { rpc } = require('../lib/cron');
const TX = require('../model/tx');
const UTXO = require('../model/utxo');
const STXO = require('../model/stxo');

function hexToString(hexx) {
  var hex = hexx.toString()//force conversion
  var str = ''
  for (var i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
    str += String.fromCharCode(parseInt(hex.substr(i, 2), 16))
  return str
}

/**
 * Process the inputs for the tx.
 * @param {Object} rpctx The rpc tx object.
 */
async function vin(rpctx, blockHeight) {
  // Setup the input list for the transaction.
  const txin = [];
  if (rpctx.vin) {
    const stxo = [];
    const txIds = new Set();
    for (const vin of rpctx.vin) {
      txin.push({
        coinbase: vin.coinbase,
        sequence: vin.sequence,
        txId: vin.txid,
        vout: vin.vout,
      });

      if (vin.coinbase) {
        continue;
      }

      const utxo_id = `${ vin.txid }:${ vin.vout }`;
      txIds.add(utxo_id);
      const utxo = await UTXO.findOne({_id: utxo_id});
      utxo._id = utxo_id;

      if (utxo) {
        stxo.push({
          _id: utxo_id,
          txId: rpctx.txid,
          blockHeight,
          address: utxo.address,
          n: vin.vout,
          value: utxo.value,
          utxo,
        })
      } else {
        throw new Error(`[FATAL] Missing UTXO for ${JSON.stringify(vin)}`);
      }
    }

    // Insert spent transactions.
    if (stxo.length) {
      await STXO.insertMany(stxo);
    }
    // Remove spent transactions.
    if (txIds.size) {
      await UTXO.remove({ _id: { $in: Array.from(txIds) } });
    }
  }
  return txin;
}

/**
 * Process the outputs for the tx.
 * @param {Object} rpctx The rpc tx object.
 * @param {Number} blockHeight The block height for the tx.
 */
async function vout(rpctx, blockHeight) {
  // Setup the outputs for the transaction.
  const txout = [];
  if (rpctx.vout) {
    const utxo = [];
    rpctx.vout.forEach((vout) => {
      var address;
      if (vout.scriptPubKey.type == 'nulldata') {
        address = "OP_RETURN "+hexToString(vout.scriptPubKey.asm.substring(10))
      } else if (vout.scriptPubKey.type == 'nonstandard') {
        address = 'NON_STANDARD'
      } else {
        address = vout.scriptPubKey.addresses[0]
      }

      const to = {
        blockHeight,
        address: address,
        n: vout.n,
        value: vout.value
      };

      txout.push(to);
      utxo.push({
        ...to,
        _id: `${ rpctx.txid }:${ vout.n }`,
        txId: rpctx.txid
      });
    });

    // Insert unspent transactions.
    if (utxo.length) {
      await UTXO.insertMany(utxo);
    }
  }
  return txout;
}

/**
 * Process a proof of stake block.
 * @param {Object} block The block model object.
 * @param {Object} rpctx The rpc object from the node.
 */
async function addBlockTX(block, rpctx) {
  const txin = await vin(rpctx, block.height);
  const txout = await vout(rpctx, block.height);

  await TX.create({
    _id: rpctx.txid,
    blockHash: block.hash,
    blockHeight: block.height,
    createdAt: block.createdAt,
    txId: rpctx.txid,
    version: rpctx.version,
    vin: txin,
    vout: txout
  });
}

/**
 * Will process the tx from the node and return.
 * @param {String} tx The transaction hash string.
 */
async function getTX(txhash) {
  return await rpc.call('getrawtransaction', [txhash, true]);
  //const hex = await rpc.call('getrawtransaction', [txhash]);
  //return await rpc.call('decoderawtransaction', [hex]);
}

module.exports = {
  addBlockTX,
  getTX,
  vin,
  vout
};
