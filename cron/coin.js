
require('babel-polyfill');
const config = require('../config');
const { exit, rpc } = require('../lib/cron');
const fetch = require('../lib/fetch');
const locker = require('../lib/locker');
const moment = require('moment');
// Models.
const Coin = require('../model/coin');
const UTXO = require('../model/utxo');

/**
 * Get the coin related information including things
 * like price coinmarketcap.com data.
 */
async function syncCoin() {
  const date = moment().utc().startOf('minute').toDate();
  // Setup the coinmarketcap.com api url.
  const url = `${ config.coinMarketCap.api }${ config.coinMarketCap.ticker }`;

  const info = await rpc.call('getinfo');
  const masternodelist = await rpc.call('masternodelist');
  var mnsOff = 0
  var mnsOn = 0
  for (let key in masternodelist) {
    var status = masternodelist[key].status;
    if (status == "ENABLED") {
      mnsOn++;
    } else {
      mnsOff++;
    }
  }
  // const masternodes = Object.keys(masternodelist).length;
  // console.log(`masternodes length: ${masternodes}`)
  const nethashps = await rpc.call('getnetworkhashps');

  let market = await fetch(url);
  if (Array.isArray(market)) {
    market = market.length ? market[0] : {};
  }

  const earndrop_res = await UTXO
    .aggregate()
    .match({ address: { $in: config.coin.earndrops } })
    .group({
      _id: null,
      balance: { $sum: '$value' },
    })
    .allowDiskUse(true)
    .exec();
  const treasury_res = await UTXO
    .aggregate()
    .match({ address: config.coin.treasury })
    .group({
      _id: null,
      balance: { $sum: '$value' },
    })
    .allowDiskUse(true)
    .exec();

  const MN_COLLATERAL_AMMOUNT = 10000;
  const supply = market.available_supply;
  const earndrop = earndrop_res.length && earndrop_res[0].balance || 0;
  const treasury = treasury_res.length && treasury_res[0].balance || 0;
  const locked = (mnsOff + mnsOn) * MN_COLLATERAL_AMMOUNT;
  const circulating = supply - earndrop - treasury - locked;

  const coin_data = {
    cap: market.market_cap_usd,
    createdAt: date,
    blocks: info.blocks,
    btc: market.price_btc,
    diff: info.difficulty,
    mnsOff: mnsOff,
    mnsOn: mnsOn,
    netHash: nethashps,
    peers: info.connections,
    status: 'Online',
    supply,
    earndrop,
    treasury,
    locked,
    circulating,
    usd: market.price_usd,
  };

  const coin = new Coin(coin_data);

  await coin.save();
}

/**
 * Handle locking.
 */
async function update() {
  const type = 'coin';
  let code = 0;

  try {
    locker.lock(type);
    await syncCoin();
  } catch(err) {
    console.log(err);
    code = 1;
  } finally {
    try {
      locker.unlock(type);
    } catch(err) {
      console.log(err);
      code = 1;
    }
    exit(code);
  }
}

update();
