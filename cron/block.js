
require('babel-polyfill');
const blockchain = require('../lib/blockchain');
const { exit, rpc } = require('../lib/cron');
const { forEachSeries } = require('p-iteration');
const locker = require('../lib/locker');
const util = require('./util');
// Models.
const Block = require('../model/block');
const TX = require('../model/tx');
const UTXO = require('../model/utxo');
const STXO = require('../model/stxo');
const Balance = require('../model/balance');
const Spend = require('../model/spend');

const FULL_BALANCE_SYNC_EVERY = 20;
const LARGE_RESYNC_PERIODIC_BALANCES = 100;

async function syncBalances(height, incremental_start, allow_full=true, force_full=false) {
  // NOTE: there are many way how incremental updates may go wrong in MongoDB.
  //       A periodic full resync is essential.
  const do_full = force_full || allow_full && ((height % FULL_BALANCE_SYNC_EVERY) == 0);

  if (do_full) {
    await UTXO
      .aggregate([
        { $group: {
            _id: '$address',
            balance: { $sum: '$value' },
          }
        },
        { $out: 'balances'},
      ])
      .allowDiskUse(true);

    await STXO
      .aggregate([
        { $group: {
            _id: '$address',
            spent: { $sum: '$value' },
          }
        },
        { $out: 'spends'},
      ])
      .allowDiskUse(true);
  } else {
    const utxo_sum = await UTXO
      .aggregate([
        { $match: { blockHeight: { $gte : incremental_start } } },
        { $group: {
            _id: '$address',
            change: { $sum: '$value' },
          }
        },
      ])
      .allowDiskUse(true);

    const stxo_sum = await STXO
      .aggregate([
        { $match: { blockHeight: { $gte : incremental_start } } },
        { $group: {
            _id: '$address',
            change: { $sum: '$value' },
          }
        },
      ])
      .allowDiskUse(true);

    // Special case when period covers new UTXOs consumed by STXOs
    const stxo_utxo_sum = await STXO
      .aggregate([
        { $match: {
            blockHeight: { $gte : incremental_start },
            'utxo.blockHeight': { $gte : incremental_start },
          }
        },
        { $group: {
            _id: '$address',
            change: { $sum: '$utxo.value' },
          }
        },
      ])
      .allowDiskUse(true);

    const balance_ops = [];
    const spend_ops = [];

    utxo_sum.forEach( utxo => {
      balance_ops.push({
        updateOne : {
          filter: { _id: utxo._id },
          update: {
            $inc: { balance: utxo.change },
          },
          upsert: true,
        }
      });
    });
    stxo_utxo_sum.forEach( utxo => {
      balance_ops.push({
        updateOne : {
          filter: { _id: utxo._id },
          update: {
            $inc: { balance: utxo.change },
          },
          upsert: true,
        }
      });
    });
    stxo_sum.forEach( stxo => {
      balance_ops.push({
        updateOne : {
          filter: { _id: stxo._id },
          update: {
            $inc: { balance: -stxo.change },
          },
        }
      });
      spend_ops.push({
        updateOne : {
          filter: { _id: stxo._id },
          update: {
            $inc: { spent: stxo.change },
          },
          upsert: true,
        }
      });
    });

    if (balance_ops.length) {
      await Balance.bulkWrite(balance_ops, { ordered: false });
    }

    if (spend_ops.length) {
      await Spend.bulkWrite(spend_ops, { ordered: false });
    }
  }

  await Balance.aggregate([
    { $sort: { balance: -1 } },
    { $limit: 100 },
    { $project: {
        _id: false,
        address: '$_id',
        value: '$balance',
      },
    },
    { $out: 'rich'},
  ]);

  console.log(`Synced balances at ${height} ${do_full ? '(full)' : '(incremental)'}`);
}

/**
 * Process the blocks and transactions.
 * @param {Number} start The current starting block height.
 * @param {Number} stop The current block height at the tip of the chain.
 */
async function syncBlocks(start, stop, last_block_hash) {
  if (true) {
    // Make sure to force cleanup on abort !
    await Block.update({ height: { $gte: start } }, { complete: false });

    // Undo UTXOs removal
    const utxo_undo = await STXO
      .aggregate([
        { $match: { blockHeight: { $gte: start } } },
        { $replaceRoot: { newRoot: '$utxo' } },
        { $match: { blockHeight: { $lt: start } } },
        // This drops collection, "mode" is not supported in 3.6
        //{ $out: 'utxo' }
      ])
      .allowDiskUse(true)
      .exec();

    if (utxo_undo.length) {
      console.log(`[INFO] Undoing ${utxo_undo.length} STXO->UTXO`);
      UTXO.insertMany(utxo_undo);
      utxo_undo.lenth = 0;
    }

    await TX.remove({ blockHeight: { $gte: start } });
    await UTXO.remove({ blockHeight: { $gte: start } });
    await STXO.remove({ blockHeight: { $gte: start } });

    // Make sure the last to be removed !
    const res = await Block.remove({ height: { $gte: start } });

    if (res.n > 0) {
      await syncBalances(start, 0, true, true);
    }
  }

  let height = start;
  let incremental_start = start;

  for (; height <= stop; height++) {
    const hash = await rpc.call('getblockhash', [height]);
    const rpcblock = await rpc.call('getblock', [hash]);
    const prev = rpcblock.height > 1 ? rpcblock.previousblockhash : 'GENESIS';

    if (last_block_hash !== prev) {
      throw new Error(`[INFO] Aborting on detected reorganization ${last_block_hash} != ${prev}!`);
    }

    const block = new Block({
      hash,
      height,
      bits: rpcblock.bits,
      createdAt: new Date(rpcblock.time * 1000),
      diff: rpcblock.difficulty,
      merkle: rpcblock.merkleroot,
      nonce: (rpcblock.nonce !== undefined) ? rpcblock.nonce : rpcblock.stakemod,
      prev,
      size: rpcblock.size,
      txs: rpcblock.tx ? rpcblock.tx : [],
      ver: rpcblock.version
    });

    await block.save();

    await forEachSeries(block.txs, async (txhash) => {
      const rpctx = await util.getTX(txhash);
      await util.addBlockTX(block, rpctx);
    });

    await Block.findOneAndUpdate({ hash }, { complete: true });

    console.log(`Height: ${ block.height } Hash: ${ block.hash }`);
    last_block_hash = block.hash;

    if ((height % LARGE_RESYNC_PERIODIC_BALANCES) == 0) {
      // Ensure to progressively update balances on large sync
      // May be double run with the one below.
      await syncBalances(height, incremental_start, false);
      incremental_start = height + 1;
    }
  }

  await syncBalances(height, incremental_start, true);
}

async function findForkPoint(info) {
  const rpcheight = info.blocks;
  const rpc_tip = await Block.findOne({ hash: info.bestblockhash, complete: true });
  const own_tip = await Block.findOne({ complete: true }).sort({ height: -1});

  if (!own_tip) {
    console.log(`[WARN] empty database !`);
    return own_tip;
  }

  for (let height = own_tip.height; height > 0; --height) {
    const blocks = await Block.find({ height, complete: true });

    if (blocks.length != 1) {
      throw new Error(
        (blocks.length > 1)
        ? `[FATAL] More than one block with the same height: ${JSON.stringify(blocks)}`
        : `[FATAL] Block at ${height} is missing in database`);
    }

    const block = blocks[0];

    // Special case, if RPC is syncing from scratch
    if ((block.height > rpcheight) && rpc_tip) {
      console.log(`[WARN] Database is ahead of RPC!`);
      return block;
    }

    let rpc_block_info;

    try {
      rpc_block_info = await rpc.call('getblock', [block.hash]);
    } catch (e) {
      if (e.message === 'Block not found') {
        console.log(`[WARN] Unknown orphan block ${block.hash} is detected at height ${height} !`);
        continue;
      } else {
        throw e;
      }
    }

    // Found fork point
    if ((rpc_block_info.height === block.height) && (rpc_block_info.confirmations >= 0)) {
      //console.log(`[INFO] found fork point: ${block}`);
      return block;
    }

    console.log(`[WARN] Orphan block ${block.hash} is detected at height ${height} !`);
  }

  return null;
}

/**
 * Handle locking.
 */
async function update() {
  const type = 'block';
  let code = 0;

  try {
    locker.lock(type);

    const info = await rpc.call('getblockchaininfo');
    const block = await findForkPoint(info);

    let start = block && block.height
        ? block.height + 1
        : 1;
    let rpcHeight = info.blocks;

    // If heights provided then use them instead.
    if (process.argv[2] !== '' && !isNaN(process.argv[2])) {
      start = Math.min(start, parseInt(process.argv[2], 10));
      Object.assign(block, await Block.findOne({ height: start - 1 }));
    }
    if (process.argv[3] !== '' && !isNaN(process.argv[3])) {
      rpcHeight = Math.min(rpcHeight, parseInt(process.argv[3], 10));
    }
    console.log(start, rpcHeight);
    // If nothing to do then exit.
    if (start > rpcHeight) {
      return;
    }
    // If starting from genesis skip.
    else if (start === 0) {
      // NOTE: we skip genesis block!
      start = 1;
    }

    await syncBlocks(start, rpcHeight, block ? block.hash : 'GENESIS');
  } catch(err) {
    console.log(err);
    code = 1;
  } finally {
    try {
      locker.unlock(type);
    } catch(err) {
      console.log(err);
      code = 1;
    }
    exit(code);
  }
}

update();
