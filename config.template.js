
/**
 * Global configuration object.
 */
const config = {
  'api': {
    'host': 'http://localhost:3000',
    'port': process.env.PORT,
    'prefix': '/api',
    'timeout': '35s'
  },
  'coinMarketCap': {
    'api': 'http://api.coinmarketcap.com/v1/ticker/',
    'ticker': 'energi'
  },
  'db': {
    'host': '127.0.0.1',
    'port': '27017',
    'name': 'blockex',
    'user': 'blockexuser',
    'pass': 'blockex'
  },
  'freegeoip': {
    'api': 'https://extreme-ip-lookup.com/json/'
  },
  'rpc': {
    'host': 'localhost',
    'port': '9796',
    'user': 'energirpc',
    'pass': 'energirpc',
    'timeout': 8000, // 8 seconds
  },
  // 'faucet': {
  //   'wait_time': 1440,
  //   'percent': 0.02,
  //   'limit': 500
  // },
  'coin':{
    'testnet':false,
    'treasury': 'EVdNtWbQ2VL41VM9AdE6QP7WrJ59tSEV9A',
    'earndrops': [
      'EKSkhKJFZM6Ln2ERPXzfRKnLW34zPUYfng',
      'Ea5i3sG6SPHon5zD1vC3cSaKijLSoGh2ti',
    ],
  },
  'coin_test':{
    'testnet': true,
    'treasury': 'EVdNtWbQ2VL41VM9AdE6QP7WrJ59tSEV9A',
    'earndrops': [
    ],
  }
};

module.exports = config;
